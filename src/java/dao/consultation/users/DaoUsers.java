
package dao.consultation.users;

import entites.Users;

public interface DaoUsers {
    
    Users getUser(String login);
    
}
