package web.servicerest;

import dto.client.DtoClient;
import dto.commande.DtoCommande;
import dto.lignedecommande.DtoLigneDeCommande;
import dto.region.DtoRegion;
import entites.Client;
import entites.Commande;
import entites.LigneDeCommande;
import entites.Produit;
import entites.Region;
import entites.Users;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import utilitaires.UtilDate;

@Stateless
@Path("gc")
public class WebServRest {
    
    @Inject dao.consultation.commande.DaoCommande daocom;
    @Inject dao.consultation.client.DaoClient daocli;
    @Inject dao.consultation.region.DaoRegion daoreg;
    @Inject dao.consultation.users.DaoUsers daousers;
    
    @GET
    @Path("user/{login}/{mdp}")
    @Produces({"text/plain"})
    public String getUser(@PathParam("login")String login, @PathParam("mdp") String mdp) {
        
        String retour;
        Users usr = daousers.getUser(login);
        
        if(usr != null){
            if(usr.getMdp().equals(mdp)){
                retour = "Ok";
            }
            else retour="PasOk";
        }
        else retour="PasOk";
        
        return retour;
    }
    
    @GET
    @Path("client/{email}/{mdp}")
    @Produces({"text/plain"})
    public String getClientCo(@PathParam("email")String email, @PathParam("mdp") String mdp) {
        
        List<Client> lesCli = daocli.getTousLesClients();
        String retour = "";
        for(Client unCli : lesCli){
            
            if(unCli.getEmail().equals(email) && unCli.getMdp().equals(mdp)){
                
                retour = unCli.getNumCli().toString();
            }
        }
        
        return retour;
    }
 
    @GET
    @Path("resumecommande/{numerocommande}")
    @Produces({"application/xml","application/json"})
    public DtoCommande getCommande(@PathParam("numerocommande")Long numcom) {
       
        DtoCommande dto = new DtoCommande();
        Commande cmd = daocom.getCommande(numcom);
        
        dto.setEmail(cmd.getLeClient().getEmail());
        dto.setMdp(cmd.getLeClient().getMdp());
        dto.setNumcom(cmd.getNumCom());
        dto.setDatecom(UtilDate.format(cmd.getDateCom()));
        dto.setNumcli(cmd.getLeClient().getNumCli());
        dto.setNomcli(cmd.getLeClient().getNomCli());
        dto.setAdrcli(cmd.getLeClient().getAdrCli());
        dto.setCodereg(cmd.getLeClient().getLaRegion().getCodeRegion());
        dto.setNomreg(cmd.getLeClient().getLaRegion().getNomRegion());
        dto.setEtatcom(cmd.getEtatCom());
        dto.setMontantcommandeht(cmd.montantCommandeHT());
        dto.setMontantCommandeTTC(cmd.montantCommandeTTC());
        dto.setMontanttva(cmd.montantTVA());
        dto.setTauxrem(cmd.getLeClient().getTauxRemCli());
        dto.setTotalNet(cmd.totalNet());

        
        return dto;
        
    }
    
    @GET
    @Path("resumescommande/{numeroclient}")
    @Produces({"application/xml","application/json"})
    public List<DtoClient> getClient(@PathParam("numeroclient")Long numcli) {
       
        
        Client client = daocli.getLeClient(numcli);
        List<Commande> lesCom = client.getLesCommandes();
        List<DtoClient> dto = new LinkedList();
        Produit prod = new Produit();
        
        for(Commande cmd : lesCom){
            DtoClient dtoClient = new DtoClient();
            List<LigneDeCommande> lesLignes = cmd.getLesLignesDeCommande();
            
                for(LigneDeCommande ligne : lesLignes){
                    prod = ligne.getLeProduit();
                }
            dtoClient.setNumCom(cmd.getNumCom());
            dtoClient.setDateCom(cmd.getDateCom());
            dtoClient.setDesigProd(prod.getDesigProd());
            dtoClient.setEtatCom(cmd.getEtatCom());
            dtoClient.setNomCateg(prod.getLaCategorie().getNomCateg());
            dtoClient.setPrixProd(prod.getPrixProd());
            
            dto.add(dtoClient);
        }
        
        return dto;
        
    }
    
    @GET
    @Path("resumeslignesdecommande/{numerocommande}")
    @Produces({"application/xml","application/json"})
    public List<DtoLigneDeCommande> getLignesCommande(@PathParam("numerocommande")Long numcom) {
       
        List<DtoLigneDeCommande> lesLignes = new LinkedList();
        
        Commande cmd = daocom.getCommande(numcom);
        
        for( LigneDeCommande ldc : cmd.getLesLignesDeCommande()){
           
            
            DtoLigneDeCommande dto = new DtoLigneDeCommande();
            
            dto.setDesigPro(ldc.getLeProduit().getDesigProd());
            dto.setRefProduit(ldc.getLeProduit().getRefProd());
            dto.setQteCommandee(ldc.getQteCom());
            dto.setPrixUnitaire(ldc.getLeProduit().getPrixProd());
            dto.setMontantHTLigne(ldc.montantDeLigneComHT());
            dto.setMontantTTCLigne(ldc.montantDeLigneComTTC());
            
            lesLignes.add(dto);
        }
        
       
        
        return lesLignes;
        
    }
    
    @GET
    @Path("resumeregion/{coderegion}")
    @Produces({"application/xml","application/json"})
    public DtoRegion getRegion(@PathParam("coderegion")String codereg) {
       
        DtoRegion dto = new DtoRegion();
        Region reg = daoreg.getLaRegion(codereg);
        chargerDto(dto, reg);
        
        return dto;
        
    }
    
    @GET
    @Path("resumesregion/tous")
    @Produces({"application/xml","application/json"})
    public List<DtoRegion> getLesRegions() {
       
        
        
        List<Region> lesRegions = daoreg.getToutesLesRegions();
        List<DtoRegion> lesDto = new LinkedList();
        
        for(Region reg : lesRegions){
            DtoRegion dto = new DtoRegion();
            chargerDto(dto, reg);
            
            lesDto.add(dto);
        }
        
        return lesDto;
        
    }

    private void chargerDto(DtoRegion dto, Region reg) {
        dto.setCodeReg(reg.getCodeRegion());
        dto.setNomReg(reg.getNomRegion());
        dto.setNbClient(reg.getLesClients().size());
        dto.setCaAnnuel(reg.caAnneeEnCours());
    }
   
}
