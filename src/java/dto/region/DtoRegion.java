package dto.region;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DtoRegion {
    
    private String codeReg;
    private String nomReg;
    private Float caAnnuel;
    private int nbClient;

    
    //<editor-fold defaultstate="collapsed" desc="Getters & Setters">
    
    public String getCodeReg() {
        return codeReg;
    }

    public void setCodeReg(String codeReg) {
        this.codeReg = codeReg;
    }

    public String getNomReg() {
        return nomReg;
    }

    public void setNomReg(String nomReg) {
        this.nomReg = nomReg;
    }
        public Float getCaAnnuel() {
        return caAnnuel;
    }

    public void setCaAnnuel(Float caAnnuel) {
        this.caAnnuel = caAnnuel;
    }

    public int getNbClient() {
        return nbClient;
    }

    public void setNbClient(int nbClient) {
        this.nbClient = nbClient;
    }
    
    //</editor-fold>  
    
}
