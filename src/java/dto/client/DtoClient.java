package dto.client;

import entites.Commande;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DtoClient {
    
    private Long numCom;
    private Date dateCom;
    private String etatCom;
    private String desigProd;
    private Float  prixProd;
    private String nomCateg;

    //<editor-fold defaultstate="collapsed" desc="Getters & Setters">
    
   

    public Date getDateCom() {
        return dateCom;
    }

    public Long getNumCom() {
        return numCom;
    }

    public void setNumCom(Long numCom) {
        this.numCom = numCom;
    }

    public void setDateCom(Date dateCom) {
        this.dateCom = dateCom;
    }

    public String getEtatCom() {
        return etatCom;
    }

    public void setEtatCom(String etatCom) {
        this.etatCom = etatCom;
    }

    public String getDesigProd() {
        return desigProd;
    }

    public void setDesigProd(String desigProd) {
        this.desigProd = desigProd;
    }

    public Float getPrixProd() {
        return prixProd;
    }

    public void setPrixProd(Float prixProd) {
        this.prixProd = prixProd;
    }

    public String getNomCateg() {
        return nomCateg;
    }

    public void setNomCateg(String nomCateg) {
        this.nomCateg = nomCateg;
    }
    
    
    //</editor-fold>  
    
}
