package dto.lignedecommande;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DtoLigneDeCommande {
    
    private String    refProduit;
    private String  desigPro;
    private Float   qteCommandee;
    private Float   prixUnitaire;
    private Float   montantHTLigne;
    private Float   montantTTCLigne;

    //<editor-fold defaultstate="collapsed" desc="Getters & Setters">
    
    public String getRefProduit() {
        return refProduit;
    }

    public void setRefProduit(String refProduit) {
        this.refProduit = refProduit;
    }

    public String getDesigPro() {
        return desigPro;
    }

    public void setDesigPro(String desigPro) {
        this.desigPro = desigPro;
    }

    public Float getQteCommandee() {
        return qteCommandee;
    }

    public void setQteCommandee(Float qteCommandee) {
        this.qteCommandee = qteCommandee;
    }

    public Float getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(Float prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public Float getMontantHTLigne() {
        return montantHTLigne;
    }

    public void setMontantHTLigne(Float montantHTLigne) {
        this.montantHTLigne = montantHTLigne;
    }

    public Float getMontantTTCLigne() {
        return montantTTCLigne;
    }

    public void setMontantTTCLigne(Float montantTTCLigne) {
        this.montantTTCLigne = montantTTCLigne;
    }
    
    //</editor-fold>  
    
}