package entites;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class TrancheTarifaire implements Serializable {
    
    @Id
    private Long idTranche;
    
    private Long  qteDebutTranche;
    private Float qteFinTranche;
    private Float prixUnitaireTranche;
    
    @JoinColumn(name="CODECATEG")
    @ManyToOne
    private Produit leProduit;

    public TrancheTarifaire() {
    }
    
    public TrancheTarifaire(Long idTranche, Long qteDebutTranche, Float qteFinTranche, Float prixUnitaireTranche) {
       
        this.idTranche           = idTranche;
        this.qteDebutTranche     = qteDebutTranche;
        this.qteFinTranche       = qteFinTranche;
        this.prixUnitaireTranche = prixUnitaireTranche;
        
   }
    
   public void afficher(){
    
        System.out.printf("%-10s  %-20s  %5.2f",idTranche,qteDebutTranche,qteFinTranche,prixUnitaireTranche);
        
   }

   //<editor-fold defaultstate="collapsed" desc="GETTERS ET SETTERS">
   
    public Long getIdTranche() {
        return idTranche;
    }

    public void setIdTranche(Long idTranche) {
        this.idTranche = idTranche;
    }

    public Long getQteDebutTranche() {
        return qteDebutTranche;
    }

    public void setQteDebutTranche(Long qteDebutTranche) {
        this.qteDebutTranche = qteDebutTranche;
    }

    public Float getQteFinTranche() {
        return qteFinTranche;
    }

    public void setQteFinTranche(Float qteFinTranche) {
        this.qteFinTranche = qteFinTranche;
    }

    public Float getPrixUnitaireTranche() {
        return prixUnitaireTranche;
    }

    public void setPrixUnitaireTranche(Float prixUnitaireTranche) {
        this.prixUnitaireTranche = prixUnitaireTranche;
    }
    
    public Produit getLeProduit() {
        return leProduit;
    }

    public void setLeProduit(Produit leProduit) {
        this.leProduit = leProduit;
    }
    
    //</editor-fold>

}
