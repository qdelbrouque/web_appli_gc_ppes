
package bal;

import dao.consultation.commande.DaoCommande;
import entites.Commande;
import javax.inject.Inject;
import static utilitaires.UtilDate.*;


public class BalImpl implements Bal{
  
    @Inject DaoCommande daoCommande; 
   
    @Override
    public Float caAnnuel(int pAnnee) {
        
        Float ca=0f;
        for (Commande cmd : daoCommande.getToutesLesCommandes()){
           
            if( annee(cmd.getDateCom())== pAnnee && cmd.estReglee()){
                  ca+=cmd.montantCommandeHT();
            }
        }
        return ca;
    }

    @Override
    public Float caMensuel(int pAnnee, int pMois) {
        
        Float ca=0f;
        
        for (Commande cmd : daoCommande.getToutesLesCommandes()){
           
            if( dansAnneeEtMois(cmd.getDateCom(),pAnnee,pMois) && cmd.estReglee()){
                  ca+=cmd.montantCommandeHT();
            }
        }
        return ca;
    }
}
